# Bom Radar Gif Builder
By Jeremy

## What does it do?
Re-creates the Bureau of Meteorology's Rain Radar as a gif.
Specifically, the [64km Adelaide (Buckland Park) Radar Loop](http://www.bom.gov.au/products/IDR644.loop.shtml).

## How does it do that?
1. Grabs the rain images files from BoM's ftp (these are a transparent layer, showing only the rain and no background)
2. Keeps the 6 most recent rain images.
3. Combines these frames with the background and label layers using imagemagick
4. Creates a gif using these combined images using imagemagick

## Requirements
- `imagemagick`
- `wget`

### MacOS Install
```
brew install wget imagemagick
```

### Linux Install
```
sudo apt-get install -y wget imagemagick
```

## Thanks to
Riley, for fixing \*nix support.

